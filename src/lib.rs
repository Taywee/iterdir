use std::{error::Error, fmt, fs::DirEntry, os::raw::c_int, slice};
use std::os::unix::ffi::OsStrExt;

#[allow(warnings)]
mod ffi;

#[derive(Debug)]
struct IterDir(Option<std::fs::ReadDir>);

#[derive(Debug, Clone, Copy, Default)]
struct ClosedError;

impl fmt::Display for ClosedError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Can not operate on a closed IterDir")
    }
}

impl Error for ClosedError {
}

impl IterDir {
   fn new(iterator: std::fs::ReadDir) -> Self {
       Self(Some(iterator))
   }

   fn close(&mut self) {
       self.0 = None;
   }

   fn next(&mut self) -> Result<Option<DirEntry>, Box<dyn Error>> {
       match &mut self.0 {
           Some(read_dir) => {
               let next_entry = read_dir.next();
               Ok(next_entry.transpose()?)
           }
           None => Err(Box::new(ClosedError))
       }
   }
}

impl Drop for IterDir {
    fn drop(&mut self) {
    }
}

unsafe extern "C" fn iterdir_close(state: *mut ffi::lua_State) -> c_int {
    let iterdir = ffi::luaL_checkudata(state, 1, b"IterDir\0".as_ptr() as _) as *mut IterDir;
    (*iterdir).close();
    0
}

unsafe extern "C" fn iterdir_gc(state: *mut ffi::lua_State) -> c_int {
    let iterdir = ffi::luaL_checkudata(state, 1, b"IterDir\0".as_ptr() as _) as *mut IterDir;
    std::ptr::drop_in_place(iterdir);
    0
}

unsafe fn iterdir_call(state: *mut ffi::lua_State) -> Result<c_int, Box<dyn Error>> {
    let iterdir = ffi::luaL_checkudata(state, 1, b"IterDir\0".as_ptr() as _) as *mut IterDir;
    let next = (*iterdir).next()?;
    match next {
        Some(entry) => {
            let name = entry.file_name();
            let name_bytes = name.as_bytes();
            ffi::lua_pushlstring(state, name_bytes.as_ptr() as _, name_bytes.len() as _);
            Ok(1)
        },
        None => Ok(0),
    }
}

unsafe extern "C" fn iterdir_call_raw(state: *mut ffi::lua_State) -> c_int {
    wrap(state, iterdir_call)
}

/// Inner wrapper.
/// Wraps a Rust function that returns an error, pushes that error on the lua stack, and returns a
/// simple status that tells whether the wrapping function should throw an error or not.
unsafe fn wrap_inner(state: *mut ffi::lua_State, function: unsafe fn(*mut ffi::lua_State) -> Result<c_int, Box<dyn Error>>) -> std::result::Result<c_int, ()> {
    match function(state) {
        Ok(value) => Ok(value),
        Err(e) => {
            let display = e.to_string();
            let bytes = display.into_bytes();
            ffi::lua_pushlstring(state, bytes.as_ptr() as _, bytes.len() as _);
            Err(())
        }
    }
}

/// Outer wrapper.
/// Calls inner wrapper, extracts the string on the lua stack, and uses it to throw an error.
/// There's no safe way of doing it in pure Rust code due to the longjmp.
unsafe fn wrap(state: *mut ffi::lua_State, function: unsafe fn(*mut ffi::lua_State) -> Result<c_int, Box<dyn Error>>) -> c_int {
    match wrap_inner(state, function) {
        Ok(value) => value,
        Err(()) => {
            let string = ffi::lua_tolstring(state, -1, std::ptr::null_mut());
            ffi::luaL_error(state, b"%s\0".as_ptr() as _, string)
        }
    }
}

unsafe fn iterdir(state: *mut ffi::lua_State) -> Result<c_int, Box<dyn Error>> {
    let path = {
        let mut size: u64 = 0;
        let ptr = ffi::luaL_checklstring(state, 1, &mut size as *mut u64) as *const u8;
        let slice = slice::from_raw_parts(ptr, size as usize);
        std::str::from_utf8(slice)?
    };
    ffi::lua_settop(state, 1);
    let block = ffi::lua_newuserdatauv(state, std::mem::size_of::<IterDir>() as _, 0) as *mut IterDir;
    if ffi::luaL_newmetatable(state, b"IterDir\0".as_ptr() as _) != 0 {
        ffi::lua_pushcclosure(state, Some(iterdir_close), 0);
        ffi::lua_setfield(state, -2, b"__close\0".as_ptr() as _);
        ffi::lua_pushcclosure(state, Some(iterdir_gc), 0);
        ffi::lua_setfield(state, -2, b"__gc\0".as_ptr() as _);
        ffi::lua_pushcclosure(state, Some(iterdir_call_raw), 0);
        ffi::lua_setfield(state, -2, b"__call\0".as_ptr() as _);
    }
    block.write(IterDir::new(std::fs::read_dir(path)?));
    ffi::lua_setmetatable(state, 2);
    ffi::lua_pushnil(state);
    ffi::lua_pushnil(state);
    ffi::lua_pushvalue(state, 2);
    Ok(4)
}


pub unsafe extern "C" fn iterdir_raw(state: *mut ffi::lua_State) -> c_int {
    wrap(state, iterdir)
}

#[no_mangle]
pub unsafe extern "C" fn luaopen_libiterdir(state: *mut ffi::lua_State) -> c_int {
    ffi::lua_pushcclosure(state, Some(iterdir_raw), 0);
    1
}
