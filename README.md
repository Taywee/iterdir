# iterdir

Sample Lua directory iteration module, playing with a bindgen with Lua and Rust.

This is mostly an experiment, doing something that is easy in C++, but not
necessarily easy in Rust.
